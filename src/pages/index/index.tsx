import styles from './index.less';

export default function IndexPage() {
  return (
    <div className="IndexStyle">
      <h1 className={styles.title}>Page index</h1>
    </div>
  );
}
